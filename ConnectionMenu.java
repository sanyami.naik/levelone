package Level1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class ConnectionMenu {


    public static void main(String[] args) throws IOException {
        ArrayList<ConnectionUser> arrayList=new ArrayList<>();
        BufferedReader bufferedReader=new BufferedReader(new InputStreamReader(System.in));
        ConnectionUser connectionUser=new ConnectionUser();
        Prepaid prepaid=new Prepaid();
        while(true) {
        System.out.println("Select from the below options");
        System.out.println("0 For exit");
        System.out.println("1 For accesing the prepaid user");
        System.out.println("2 For adding a new connection");
        System.out.println("3 For displaying all the users");
        System.out.println("Enter your choice");
        int choice=Integer.parseInt(bufferedReader.readLine());

        switch(choice)
        {
            case 0:
                System.exit(0);

            case 2:
                ConnectionUser connectionUser1=new ConnectionUser();
                connectionUser1.addNewUser(bufferedReader,arrayList);

                break;

            case 1:
                prepaid.addPrepaidDetails(bufferedReader,arrayList);
                break;

            case 3:
                connectionUser.displaydetails(arrayList);
                break;

            default:
                System.out.println("Enter a valid choice");

        }
    }

    }
}
