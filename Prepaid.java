package Level1;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;

public class Prepaid extends ConnectionUser{
    int rechargeAmount;

    @Override
    public String toString() {
        return "Prepaid{" +
                "rechargeAmount=" + rechargeAmount +
                ", userName='" + userName + '\'' +
                ", emailId='" + emailId + '\'' +
                ", simType='" + simType + '\'' +
                ", validity=" + validity +
                ", mobileNumber=" + mobileNumber +
                ", balance=" + balance +
                '}';
    }

    void addPrepaidDetails(BufferedReader bufferedReader, ArrayList<ConnectionUser> arrayList) throws IOException {
        System.out.println("Enter the mobile number ");
        int mobileNumber= Integer.parseInt(bufferedReader.readLine());

            for (ConnectionUser connectionUserIndex : arrayList) {

                    if (mobileNumber == connectionUserIndex.mobileNumber && connectionUserIndex.simType=="prepaid")
                        break;

                System.out.println("Enter the options from below");
                System.out.println("0 For balance and validity");
                System.out.println("1:For recharging,updating the balance,extending the validity");
                int option=Integer.parseInt(bufferedReader.readLine());
                Prepaid prepaid=new Prepaid();
                switch(option)
                {
                    case 0:
                        prepaid.checkBalanceAndValidity(mobileNumber,arrayList);
                        break;

                    case 1:
                        prepaid.addRecharge(mobileNumber,arrayList,bufferedReader);
                }

            }


    }

    void checkBalanceAndValidity(int mobileNumber,ArrayList<ConnectionUser> arrayList) {
        System.out.println("Your details are:");
        for (ConnectionUser connectionUserIndex : arrayList)

            if (mobileNumber == connectionUserIndex.mobileNumber)
                System.out.println(connectionUserIndex);

    }


    void addRecharge(int mobileNumber,ArrayList<ConnectionUser> arrayList,BufferedReader bufferedReader ) throws IOException {
        System.out.println("Enter the amount you want to add");
        int extraAmount= Integer.parseInt(bufferedReader.readLine());
        for (ConnectionUser connectionUserIndex : arrayList)

            if (mobileNumber == connectionUserIndex.mobileNumber)
            {
               connectionUserIndex.balance=connectionUserIndex.balance+extraAmount;
               System.out.println(connectionUserIndex);
            }
        System.out.println("Amount added succesfully");

    }

}
