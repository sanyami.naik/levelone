package Level1;

import java.util.Comparator;

public class SortByAscendingNames implements Comparator<ConnectionUser> {


    @Override
    public int compare(ConnectionUser o1, ConnectionUser o2) {
        return o1.userName.compareTo(o2.userName);
    }


}
