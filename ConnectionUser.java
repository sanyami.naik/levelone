package Level1;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class ConnectionUser {

    String userName;
    String emailId;
    String simType;
    int validity=30;
    int mobileNumber;
    int balance;

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getSimType() {
        return simType;
    }

    public void setSimType(String simType) {
        this.simType = simType;
    }

    public int getValidity() {
        return validity;
    }

    public void setValidity(int validity) {
        this.validity = validity;
    }

    public int getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(int mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    @Override
    public String toString() {
        return "ConnectionUser{" +
                "userName='" + userName + '\'' +
                ", emailId='" + emailId + '\'' +
                ", simType='" + simType + '\'' +
                ", validity=" + validity +
                ", mobileNumber=" + mobileNumber +
                ", balance=" + balance +
                '}';
    }

    void addNewUser(BufferedReader bufferedReader, ArrayList<ConnectionUser>arrayList) throws IOException {


        System.out.println("Enter the details of the user");

        System.out.println("Enter the name of the user");
        String name= bufferedReader.readLine();
        setUserName(name);

        System.out.println("Enter the type of the sim");
        String simType=bufferedReader.readLine();
        setSimType(simType);

        System.out.println("Enter the email ID of the user");
        String email=bufferedReader.readLine();
        setEmailId(email);

        try {

            System.out.println("Enter the initial balance");
            int initialBalance = Integer.parseInt(bufferedReader.readLine());
            if (initialBalance < 0) {
                throw new RechargeAmountTooLowException();
            } else
                setBalance(initialBalance);
        }
        catch(RechargeAmountTooLowException c){

        }

        setValidity(30);

        setMobileNumber(ThreadLocalRandom.current().nextInt(1000000000)*10);

        arrayList.add(this);
    }


    void displaydetails(ArrayList<ConnectionUser> arrayList)
    {
        System.out.println("The details of all the users are as folows:");
        SortByAscendingNames sortByAscendingNames=new SortByAscendingNames();
        Collections.sort(arrayList,sortByAscendingNames);

        for (ConnectionUser connectionUserIndex:arrayList) {
            System.out.println(connectionUserIndex);
        }


    }


}
